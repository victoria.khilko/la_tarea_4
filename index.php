<?php
/**
 * Реализовать проверку заполнения обязательных полей формы в предыдущей
 * с использованием Cookies, а также заполнение формы по умолчанию ранее
 * введенными значениями.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');
$abilities = ['Immortality'=>'Immortality','Passing through walls'=>'Passing through walls','Levitation'=>'Levitation'];
for ($i=1900;$i<=2020;$i++){
    $year[$i] = $i;
}
// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Массив для временного хранения сообщений пользователю.
    $messages = array();
    
    // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
    // Выдаем сообщение об успешном сохранении.
    if (!empty($_COOKIE['save'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('save', '', 100000);
        // Если есть параметр save, то выводим сообщение пользователю.
        $messages[] = 'Thanks, results saved.';
    }
    
    // Складываем признак ошибок в массив.
    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    // TODO: аналогично все поля.
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['gender'] = !empty($_COOKIE['gender_error']);
    $errors['limbs'] = !empty($_COOKIE['limbs_error']);
    $errors['abilities'] = !empty($_COOKIE['abilities_error']);
    $errors['bio'] = !empty($_COOKIE['bio_error']);
    $errors['accept'] = !empty($_COOKIE['accept_error']);
    
    // Выдаем сообщения об ошибках.
    if ($errors['fio']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('fio_error', '',100000);
        // Выводим сообщение.
        if ($_COOKIE['fio_error'] == '1'){
            $messages[] = '<div class="error">No name.</div>';
        }
        if ($_COOKIE['fio_error'] == '2'){ $messages[] = '<div class="error">Name is word made from english letters.</div>';
        }
    }
    // TODO: тут выдать сообщения об ошибках в других полях.
    
    if ($errors['email']) {
        setcookie('email_error', '',100000);
        
        if ($_COOKIE['email_error'] == '1'){
            $messages[] = '<div class="error">No email.</div>';
        }
        if ($_COOKIE['email_error'] == '2'){ $messages[] = '<div class="error">Email is word how us1er@mysite.com and other.</div>';
        }
    }
    if ($errors['gender']) {
        setcookie('gender_error', '',100000);
        $messages[] = '<div class="error">No gender.</div>';
    }
    if ($errors['limbs']) {
        setcookie('limbs_error', '',100000);
        $messages[] = '<div class="error">No limbs.</div>';
    }
    if ($errors['abilities']) {
        setcookie('abilities_error', '',100000);
        $messages[] = '<div class="error">No abilities.</div>';
    }
    
    if ($errors['bio']) {
        setcookie('bio_error', '',100000);
        
        if ($_COOKIE['bio_error'] == '1'){
            $messages[] = '<div class="error">No bio.</div>';
        }
        if ($_COOKIE['bio_error'] == '2'){ $messages[] = '<div class="error">Bio is text only with a-z A-Z.</div>';
        }
    }
    
    if ($errors['accept']) {
        setcookie('accept_error', '',100000);
        $messages[] = '<div class="error">No accept.</div>';
    }
    // Складываем предыдущие значения полей в массив, если есть.
    $values = array();
    $values['fio'] = (empty($_COOKIE['fio_value']) || !preg_match('/^[a-zA-Z]/',$_COOKIE['fio_value']) ) ? '' : $_COOKIE['fio_value'];                  //dhfhbghjsg dgbwsjjvag fsvdazsdafh adyaguyvnanvdusbgtvfbjsdhybnfhgkjdbfsjxdbnkdh
    // TODO: аналогично все поля.
    //$values['year'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
    $values['email'] = (empty($_COOKIE['email_value']) || !preg_match('/^[a-zA-Z0-9_\-.]+@[a-z.]/', $_COOKIE['email_value']) ) ? '' : $_COOKIE['email_value'];
    $values['gender'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
    $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
    $values['bio'] = (empty($_COOKIE['bio_value']) || !preg_match('/^[a-zA-Z ]/',$_COOKIE['bio_value'])) ? '' : $_COOKIE['bio_value'];
    $values['accept'] = empty($_COOKIE['accept_value']) ? '' : $_COOKIE['accept_value'];
    
    if (!empty($_COOKIE['year_value'])){
        $year_value = json_decode($_COOKIE['year_value']);
    }
    $values['year'] = [];
    if (isset($year_value) && is_array($year_value)){
        foreach ($year_value as $ye){
            if (!empty($year[$ye]))
            {
                $values['year'][$ye] = $ye;
            }
        }
    }
    
    if (!empty($_COOKIE['abilities_value'])){
        $abilities_value = json_decode($_COOKIE['abilities_value']);
    }
    $values['abilities'] = [];
    if (isset($abilities_value) && is_array($abilities_value)){
        foreach ($abilities_value as $ability){
            if (!empty($abilities[$ability]))
            {
                $values['abilities'][$ability] = $ability;
            }
        }
    }
    // Включаем содержимое файла form.php.
    // В нем будут доступны переменные $messages, $errors и $values для вывода
    // сообщений, полей с ранее заполненными данными и признаками ошибок.
    include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
    // Проверяем ошибки.
    $errors = FALSE;
    if (empty($_POST['fio'])) {
        // Выдаем куку с флажком об ошибке в поле fio.
        setcookie('fio_error', '1');
        $errors = TRUE;
    }
    else {
        if (!preg_match('/^[a-zA-Z]/', $_POST['fio'])) {
            setcookie('fio_error', '2');
            $errors = TRUE;
        }
        
        else{
            
            // Сохраняем ранее введенное в форму значение на год.
            setcookie('fio_value', $_POST['fio'], time() + 60 * 60 * 24 * 365);
        }
    }
    // *************
    // TODO: тут необходимо проверить правильность заполнения всех остальных полей.
    // Сохранить в Cookie признаки ошибок и значения полей.
    // *************
    
    
    if (empty($_POST['email'])) {
        setcookie('email_error', '1');
        $errors = TRUE;
    }
    else {
        
        if (!preg_match('/^[a-zA-Z0-9_\-.]+@[a-z.]/', $_POST['email'])) {
            setcookie('email_error', '2');
            $errors = TRUE;
        }
        
        else{
            // Сохраняем ранее введенное в форму значение на год.
            setcookie('email_value', $_POST['email'], time() + 60 * 60 * 24 * 365);
        }
    }
    if (empty($_POST['gender'])) {
        // Выдаем куку с флажком об ошибке в поле fio.
        setcookie('gender_error', '1');
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на год.
        setcookie('gender_value', $_POST['gender'], time() + 60 * 60 * 24 * 365);
    }
    if (empty($_POST['limbs'])) {
        // Выдаем куку с флажком об ошибке в поле fio.
        setcookie('limbs_error', '1');
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на год.
        setcookie('limbs_value', $_POST['limbs'], time() + 60 * 60 * 24 * 365);
    }
    
    if (empty($_POST['abilities'])) {
        // Выдаем куку с флажком об ошибке в поле fio.
        setcookie('abilities_error', '1');
        $errors = TRUE;
    }
    else {
        $abilities_error = FALSE;
        // Сохраняем ранее введенное в форму значение на год.
        setcookie('abilities_value', json_encode($_POST['abilities']), time() + 60 * 60 * 24 * 365);
    }
    
    $_POST['year']=setcookie('year_value', json_encode($_POST['year']), time() + 60 * 60 * 24 * 365);
    
    if (empty($_POST['bio'])) {
        setcookie('bio_error', '1');
        $errors = TRUE;
    }
    else {
        if (!preg_match('/^[a-zA-Z ]/', $_POST['bio'])) {
            setcookie('bio_error', '2');
            $errors = TRUE;
        }
        else{
            // Сохраняем ранее введенное в форму значение на год.
            setcookie('bio_value', $_POST['bio'], time() + 60 * 60 * 24 * 365);
        }
    }
    
    if (empty($_POST['accept'])) {
        // Выдаем куку с флажком об ошибке в поле fio.
        setcookie('accept_error', '1');
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на год.
        setcookie('accept_value', $_POST['accept'], time() + 60 * 60 * 24 * 365);
    }
    
    if ($errors) {
        // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
        header('Location: index.php');
        exit();
    }
    else {
        // Удаляем Cookies с признаками ошибок.
        setcookie('fio_error', '', 100000);
        // TODO: тут необходимо удалить остальные Cookies.
        setcookie('email_error', '', 100000);
        setcookie('gender_error', '', 100000);
        setcookie('limbs_error', '', 100000);
        setcookie('abilities_error', '', 100000);
        setcookie('bio_error', '', 100000);
    }
    
    // Сохранение в XML-документ.
    // ...
    
    foreach ($_POST['year'] as $ye){
        if (!empty($ye))
        {
            $y=$ye;
        }
    }
    $abilities=serialize($_POST['abilities']);
    // Сохранение в базу данных.
    
    $user = 'u16351';
    $pass = '7947569';
    $db = new PDO('mysql:host=localhost;dbname=u16351', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    
    // Подготовленный запрос. Не именованные метки.
    try {
        $stmt = $db->prepare("INSERT INTO application SET name = ?, email = ?, year = ?, abilities = ?, gender = ?, limbs = ?, bio = ?");
        $stmt -> execute(array($_POST['fio'],$_POST['email'],$y,$abilities,$_POST['gender'],$_POST['limbs'],$_POST['bio']));
    }
    catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
    }
    
    // Сохраняем куку с признаком успешного сохранения.
    setcookie('save', '1');
    
    // Делаем перенаправление.
    header('Location: index.php');
}